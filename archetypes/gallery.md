---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true
showDate: true
tags: ["blog","pics","gallery"]

type: "gallery"

maxWidth: "800x"
clickablePhotos: true # Set 'true' to link images to the full size files.
---
