/*!
 *  Howler.js Audio Player Demo
 *  howlerjs.com
 *
 *  (c) 2013-2020, James Simpson of GoldFire Studios
 *  goldfirestudios.com
 *
 *  MIT License
 */

// Cache references to DOM elements.
var elms = [
  'track', 'timer', 'duration',
  'playBtn', 'pauseBtn', 'prevBtn', 'nextBtn',
  'playlistBtn', 'volumeBtn',
  'progress', 'loading',
  'playlist', 'list', 'volume'
];

elms.forEach(function(elm) {
  window[elm] = document.getElementById(elm);
});

/**
 * Player class containing the state of our playlist and where we are in it.
 * Includes all methods for playing, skipping, updating the display, etc.
 * @param {Array} playlist Array of objects with playlist song details ({title, file, howl}).
 */
var Player = function(playlist) {
  var self = this;

  this.playlist = playlist;
  this.index = 0;

  // Display the title of the first track.
  track.innerHTML = playlist[0].title;

  // Setup the playlist display.
  playlist.forEach(function(entry) {

    var div = document.createElement('div');

    div.className = 'list-song';
    div.innerHTML = entry.title;
    div.onclick = function() {
      self.play(playlist.indexOf(entry));
    };
    list.appendChild(div);
  });
};

Player.prototype = {
  /**
   * Play a song in the playlist.
   * @param  {Number} index Index of the song in the playlist (leave empty to play or resume current).
   */
  update: function() {
    var self = this;

    setTimeout(() => {
      self.step();
    });
  },
  play: function(index) {
    var self = this;

    if (typeof index === 'number') {

      if (self.current) {
        if (self.debounce) {
          clearTimeout(self.debounce);
        }

        self.current.stop();
      }
    }

    self.index = typeof index === 'number' ? index : self.index;
    var entry = self.playlist[self.index];

    if (!entry) {
      console.error("playlist index %d not valid", index);
      return;
    }

    console.log("starting playback %d", self.index, entry.title);

    self.current = entry.howl;
    // Create new player instance if needed
    if (!self.current) {
      self.current = entry.howl = new Howl({
        src: [ entry.file ],
        html5: true,
        preload: true,
        onplay: function(id) {
          console.log("onplay", id);

          self.update();
        },
        onplayerror: function(id) {
          console.error("error on playing", id);
        },
        onload: function(id) {
          console.log("onload", id);

          self.update();
        },
        onend: function(id) {
          console.log("onend", id);

          self.skip('next');
        },
        onpause: function(id) {
          console.log("onpause", id);

          self.update();
        },
        onstop: function(id) {
          console.log("onstop", id);

          self.update();
        },
        onseek: function(id) {
          // Start updating the progress of the track.
          console.log("onseek %d %d / %d", id, self.current.seek(), self.current.duration());

          //self.update();
        }
      });
    }

    track.innerHTML = entry.title;

    self.current.play();

    self.update();
  },

  /**
   * Pause the currently playing track.
   */
  pause: function() {
    var self = this;

    // Get the Howl we want to manipulate.
    if (!self.current)
      return;

    self.current.pause();
  },

  /**
   * Skip to the next or previous track.
   * @param  {String} direction 'next' or 'prev'.
   */
  skip: function(direction) {
    var self = this;

    // Get the next track based on the direction of the track.
    var index = 0;
    if (direction === 'prev') {
      index = self.index - 1;
      if (index < 0) {
        index = self.playlist.length - 1;
      }
    } else {
      index = self.index + 1;
      if (index >= self.playlist.length) {
        index = 0;
      }
    }

    self.play(index);
  },

  /**
   * Set the volume
   * @param  {Number} val Volume between 0 and 1.
   */
  volume: function(val) {
    // Update the global volume (affecting all Howls).
    Howler.volume(val);
  },

  /**
   * Seek to a new position in the currently playing track.
   * @param  {Number} pos Percentage through the song to skip.
   */
  seek: function(pos) {
    var self = this;

    if (!self.current || self.current.state() !== "loaded") {
      return;
    }

    var seconds = self.current.duration() * pos;

    console.log("seek current %d -> %d / %d",
      self.current.seek(), seconds, self.current.duration());

    if (self.debounce)
      clearTimeout(self.debounce);

    self.debounce = setTimeout(() => {
      console.log("debounced seek", seconds);
      self.current.seek(seconds);
    }, 200);
  },

  /**
   * The step called within requestAnimationFrame to update the playback position.
   */
  step: function() {
    var self = this;

    if (!self.current || self.current.state() !== "loaded") {
      timer.innerHTML = '-';
      duration.innerHTML = '-';

      loading.style.display = 'flex';
      playBtn.style.display = 'none';
      pauseBtn.style.display = 'none';

      console.log("step: loading");
    } else {

      console.log("step: position %d / %d", self.current.seek(), self.current.duration());

      // Determine our current seek position.
      var seek = self.current.seek();

      timer.innerHTML = self.formatTime(Math.round(seek));
      duration.innerHTML = self.formatTime(Math.round(self.current.duration()));

      progress.value = Math.floor((seek / self.current.duration()) * progress.max);
      console.log("step: progress %d / %d", progress.value, progress.max);

      if (!self.current.playing()) {
        console.log("step: not playing");

        loading.style.display = 'none';
        playBtn.style.display = 'flex';
        pauseBtn.style.display = 'none';

        return;
      }

      console.log("step: playing");

      loading.style.display = 'none';
      playBtn.style.display = 'none';
      pauseBtn.style.display = 'flex';
    }

    if (self.timeout)
      clearTimeout(self.timeout);

    self.timeout = setTimeout(() => {
      self.update();
    }, 500);
  },

  /**
   * Toggle the playlist display on/off.
   */
  togglePlaylist: function() {
    var self = this;
    var display = (playlist.style.display === 'none') ? 'block' : 'none';

    playlist.style.display = display;

    if (display !== 'none')
      playlist.showModal();
    else
      playlist.close();
  },

  /**
   * Toggle the volume display on/off.
   */
  toggleVolume: function() {
    var self = this;
    var display = (volume.style.display === 'flex') ? 'none' : 'flex';

    setTimeout(function() {
      volume.style.display = display;
    }, (display === 'flex') ? 0 : 500);
    volume.className = (display === 'flex') ? 'fadein' : 'fadeout';
  },

  /**
   * Format the time from seconds to M:SS.
   * @param  {Number} secs Seconds to format.
   * @return {String}      Formatted time.
   */
  formatTime: function(secs) {
    var minutes = Math.floor(secs / 60) || 0;
    var seconds = (secs - minutes * 60) || 0;

    return minutes + ':' + (seconds < 10 ? '0' : '') + seconds;
  }
};

